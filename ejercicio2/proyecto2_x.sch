EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ej2cese
LIBS:proyecto2_x-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title "ejercicio 2"
Date "22/11/16"
Rev "1.0"
Comp "MSE"
Comment1 "ejercicio"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4050 2800 1200 1200
U 5813F0A5
F0 "conector" 60
F1 "conector.sch" 60
F2 "TXD" I R 5250 3150 60 
F3 "RXD" O R 5250 3350 60 
F4 "RTS" I R 5250 3750 60 
F5 "CTS" O R 5250 3550 60 
$EndSheet
$Sheet
S 6350 3200 1150 1250
U 5813F0C0
F0 "RS232" 60
F1 "RS232.sch" 60
F2 "RS[1..4]" B L 6350 4200 60 
$EndSheet
Wire Wire Line
	5250 3150 5650 3150
Wire Wire Line
	5250 3350 5650 3350
Wire Wire Line
	5250 3550 5650 3550
Wire Wire Line
	5250 3750 5650 3750
Text Label 5300 3150 0    60   ~ 0
conn1
Text Label 5350 3350 0    60   ~ 0
conn2
Text Label 5350 3550 0    60   ~ 0
conn3
Text Label 5350 3750 0    60   ~ 0
conn4
Entry Wire Line
	5650 3350 5750 3450
Entry Wire Line
	5650 3550 5750 3650
Entry Wire Line
	5650 3750 5750 3850
Wire Bus Line
	5750 3250 5750 4200
Wire Bus Line
	5750 4200 6350 4200
Text Label 5850 4200 0    60   ~ 0
conn[1..4]
Entry Wire Line
	5650 3150 5750 3250
$EndSCHEMATC
